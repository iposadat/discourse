#!/bin/bash

# If there are files in /tmp/configmap that are not empty
# (overriden by a ConfigMap) copy them to /discourse/app/config
if [ -n "$(ls -A /tmp/discourse-configmap)" ]
then
  for f in /tmp/discourse-configmap/*
  do
    if [ -s $f ]
    then
      cp /tmp/discourse-configmap/* /discourse/app/config/
    fi
  done
fi

# Replace environment variables
echo "--> Overwritting env variables ..."
envsubst < /tmp/discourse-configmap/discourse.conf > /discourse/app/config/discourse.conf
echo "--> DONE"

if [ ! -d "/discourse/app/public/assets" ];
then
  # Precompile assets and run sidekiq
  bundle exec rake assets:precompile RAILS_ENV=production

  # Migrate DB
  bundle exec rake db:migrate RAILS_ENV=production
fi
# Run discourse with Puma
RAILS_ENV=production bundle exec puma -C ./config/puma.rb config.ru